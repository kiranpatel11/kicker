export const environment = {
  production: true,
  kickermodel: {
    // in production mode, kicker model is loaded from documentation pages (regularly regenerated)
    url: '/doc/kicker-aggregated.json'
  },
  matomo: {
    enable: false,
    url: 'https://matomo.host/',
    id: 666
  }
};
