import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {ShowdownModule} from 'ngx-showdown';
// import {MatomoModule} from 'ngx-matomo';

import {AppComponent} from './app.component';
import {KickerComponent} from './kicker/kicker.component';
import {GitlabCiComponent} from './gitlab-ci/gitlab-ci.component';
import {TemplatesSectionComponent} from './templates-section/templates-section.component';
import {StepbarComponent} from './stepbar/stepbar.component';
import {VariableEditorComponent} from './variable-editor/variable-editor.component';
import {OptionsBoxComponent} from './options-box/options-box.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    KickerComponent,
    GitlabCiComponent,
    TemplatesSectionComponent,
    StepbarComponent,
    VariableEditorComponent,
    OptionsBoxComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ShowdownModule.forRoot(
      {emoji: true, noHeaderId: true, openLinksInNewWindow: true}
    ),
    // MatomoModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
