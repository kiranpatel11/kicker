import {Component} from '@angular/core';
// import {MatomoInjector} from 'ngx-matomo';
// import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'kicker';
  public isMenuCollapsed  = true;

  // constructor(
  //   private matomoInjector: MatomoInjector
  // ) {
  //   if (environment.matomo.enable) {
  //     this.matomoInjector.init(environment.matomo.url, environment.matomo.id);
  //   }
  // }
}
