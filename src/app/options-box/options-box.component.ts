import {Component, Input} from '@angular/core';
import {Extension, Options} from '../kicker';
// import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-options-box',
  templateUrl: './options-box.component.html',
  styleUrls: ['./options-box.component.css']
})
export class OptionsBoxComponent {

  @Input() extensions: Extension[];

  @Input() options: Options;

  // constructor(
  //   private matomoTracker: MatomoTracker
  // ) {
  // }

  trackEvent(action: string, name?: string) {
    console.log('event', action, name);
    // this.matomoTracker.trackEvent('kicker', action, name);
  }

}
